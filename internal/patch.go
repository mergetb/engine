package internal

import (
	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func ApplyPatch(from, to []string, strat *portal.PatchStrategy) []string {

	res := []string{}
	switch strat.Strategy {
	case portal.PatchStrategy_replace:
		res = from
	case portal.PatchStrategy_remove:
		break
	case portal.PatchStrategy_expand:
		seen := make(map[string]bool)
		for _, r := range to {
			seen[r] = true
			res = append(res, r)
		}

		for _, r := range from {
			if !seen[r] {
				res = append(res, r)
			}
		}
	case portal.PatchStrategy_subtract:
		seen := make(map[string]bool)
		for _, r := range from {
			seen[r] = true
		}

		for _, r := range to {
			if !seen[r] {
				res = append(res, r)
			}
		}
	}

	return res
}
