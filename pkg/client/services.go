package client

import (
	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/internal"
	me "gitlab.com/mergetb/portal/services/pkg/merror"
	"google.golang.org/grpc"
)

func WorkspaceClient() (*grpc.ClientConn, portal.WorkspaceClient, error) {

	conn, err := grpc.Dial(
		"workspace:6000",
		grpc.WithInsecure(),
		internal.GRPCMaxMessage,
	)
	if err != nil {
		conn.Close()
		return nil, nil, me.SvcConnectionError("workspace", err).Log()
	}

	return conn, portal.NewWorkspaceClient(conn), nil

}

func Workspace(f func(portal.WorkspaceClient) error) error {

	conn, wkc, err := WorkspaceClient()
	if err != nil {
		return err
	}
	defer conn.Close()

	return f(wkc)

}
