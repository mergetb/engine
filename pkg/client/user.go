package client

import (
	"gitlab.com/mergetb/api/portal/v1/go"
	me "gitlab.com/mergetb/portal/services/pkg/merror"
)

func GetUser(name string) (*portal.User, error) {

	var user *portal.User
	err := Workspace(func(xpc portal.WorkspaceClient) error {

		var err error
		resp, err := xpc.GetUser(ctx, &portal.GetUserRequest{Username: name})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}
		user = resp.User

		return nil

	})
	if err != nil {
		return nil, err
	}

	return user, nil
}

func ListUsers() ([]*portal.User, error) {

	var users []*portal.User
	err := Workspace(func(xpc portal.WorkspaceClient) error {

		resp, err := xpc.GetUsers(ctx, &portal.GetUsersRequest{})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}

		users = resp.Users
		return nil

	})
	if err != nil {
		return nil, err
	}

	return users, nil
}

func InitUser(user, email string) error {

	return Workspace(func(xpc portal.WorkspaceClient) error {

		_, err := xpc.InitUser(ctx, &portal.InitUserRequest{
			Username: user,
		})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}

		return nil

	})

}

func UpdateUser(user *portal.User) error {

	// TODO
	return nil

}

func DeleteUser(user string) error {

	err := FreeUserResources(user)
	if err != nil {
		return err
	}

	return Workspace(func(xpc portal.WorkspaceClient) error {

		_, err := xpc.DeleteUser(ctx, &portal.DeleteUserRequest{User: user})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}

		return nil

	})

}

func ReadUserPublicKeys(user string) ([]*portal.PublicKey, error) {

	var keys []*portal.PublicKey
	err := Workspace(func(xpc portal.WorkspaceClient) error {

		resp, err := xpc.GetUserPublicKeys(ctx, &portal.GetUserPublicKeysRequest{
			User: user,
		})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}

		keys = resp.Keys
		return nil

	})
	if err != nil {
		return nil, err
	}

	return keys, nil

}

func AddUserPublicKey(user, key string) error {

	return Workspace(func(xpc portal.WorkspaceClient) error {

		_, err := xpc.AddUserPublicKey(ctx, &portal.AddUserPublicKeyRequest{
			User: user,
			Key:  key,
		})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}

		return nil

	})

}

func DeleteUserPublicKey(user, fingerprint string) error {

	return Workspace(func(xpc portal.WorkspaceClient) error {

		_, err := xpc.DeleteUserPublicKey(ctx, &portal.DeleteUserPublicKeyRequest{
			User:        user,
			Fingerprint: fingerprint,
		})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}

		return nil

	})

}
