package client

import (
//"gitlab.com/mergetb/api/portal/v1/go"
//me "gitlab.com/mergetb/portal/services/pkg/merror"
)

/*TODO
func ListXdcs(proj string) ([]*xc.XdcInfo, error) {

	var xdcs []*xc.XdcInfo
	err := Xdc(func(xcc xc.XdcClient) error {

		resp, err := xcc.List(ctx, &xc.ListRequest{
			Project: proj,
		})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}

		xdcs = resp.Xdcs
		return nil

	})
	if err != nil {
		return nil, err
	}

	return xdcs, nil

}
*/

func SpawnXdc(user, proj, xdc string) error {

	return nil

	/*TODO

	return Xdc(func(xcc xc.XdcClient) error {

		_, err := xcc.Spawn(ctx, &xc.SpawnRequest{
			User:    user,
			Project: proj,
			Name:    xdc,
		})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}

		return nil

	})
	*/

}

func DestroyXdc(user, proj, xdc string) error {

	/*TODO
	return Xdc(func(xcc xc.XdcClient) error {

		_, err := xcc.Destroy(ctx, &xc.DestroyRequest{
			User:    user,
			Project: proj,
			Name:    xdc,
		})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}

		return nil

	})
	*/

	return nil
}

func ReadXdcToken(proj, xdc string) (string, error) {
	/*TODO

	var token string
	err := Workspace(func(xpc xp.WorkspaceClient) error {

		resp, err := xpc.XdcToken(ctx, &xp.XdcTokenRequest{
			Project: proj,
			Name:    xdc,
		})
		if err != nil {
			return me.FromGRPCError(err).Log()
		}

		token = resp.Token
		return nil

	})
	if err != nil {
		return "", err
	}

	return token, nil
	*/
	return "", nil

}
