package identity

import (
	"fmt"
	"net/http"
	"net/url"

	ory "github.com/ory/kratos-client-go"
	"github.com/peterhellberg/link"
)

func (i *Identity) FromOryID(oryId *ory.Identity) error {

	// TODO: download the schema in id.SchemaURL, decode traits, and confirm
	// the data we need is there. For now, assume email + username exist.

	// log.Debugf("traits: %+v", oryId.Traits)

	oryTraits, ok := oryId.Traits.(map[string]interface{})
	if !ok {
		return fmt.Errorf("Unknown identity traits type")
	}

	i.Id = oryId.Id

	return i.Traits.Read(oryTraits)
}

func (i *IdentityTraits) Read(oryTraits map[string]interface{}) error {

	// generics would be nice here.
	getStr := func(key string) string {
		if val, ok := oryTraits[key]; ok {
			r, ok := val.(string)
			if ok {
				return r
			}
		}
		return ""
	}

	i.Email = getStr("email")
	i.Username = getStr("username")

	for k, trait := range oryTraits {

		if k == "email" || k == "username" {
			continue
		}

		if val, ok := trait.(string); ok {
			i.Traits[k] = val
		}
	}

	return nil
}

// GetNextPageToken parses the link header in the response to get the next page of the API
// See: https://www.ory.sh/docs/ecosystem/api-design#pagination
// Stolen from Ory Slack
func GetNextPageToken(response *http.Response) (string, error) {
	links := link.ParseResponse(response)
	if nextPage, ok := links["next"]; ok {
		uri, err := url.Parse(nextPage.URI)
		if err != nil {
			return "", fmt.Errorf("failed to parse nextPage uri: %w", err)
		}

		pageToken := uri.Query().Get("page_token")
		if pageToken == "" {
			return "", fmt.Errorf("next link present, but has no page_token")
		}

		return pageToken, nil
	} else {
		// There is no next page link, so we are at the last page of the response.
		return "", nil
	}
}
