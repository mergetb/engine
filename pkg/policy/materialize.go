package policy

import (
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// MaterializationObject ...
type MaterializationObject struct {
	Pid, Eid, Rid string
}

// UserRoles ...
func (o MaterializationObject) UserRoles(u *portal.User) ([]RoleBinding, error) {

	roles, err := ProjectAndExperimentRoles(u, o.Pid, o.Eid)
	if err != nil {
		return nil, err
	}

	r := storage.NewRealizeRequest(o.Rid, o.Eid, o.Pid)
	err = r.Read()
	if err != nil {
		return nil, err
	}

	if r.Creator == u.Username {
		roles = append(roles, RoleBinding{RealizationScope, CreatorRole})
	}

	if u.Admin {
		roles = append(roles, RoleBinding{MaterializationScope, CreatorRole})
	}

	return roles, nil
}
func Materialize(caller *identity.IdentityTraits, pid, eid, rid string) error {

	policy := GetPolicy().Materialization[Mode(Public)].Create
	return Authorize(caller, policy, MaterializationObject{pid, eid, rid})
}

func Dematerialize(caller *identity.IdentityTraits, pid, eid, rid string) error {

	policy := GetPolicy().Materialization[Mode(Public)].Destroy
	return Authorize(caller, policy, MaterializationObject{pid, eid, rid})
}

func ReadMaterialization(caller *identity.IdentityTraits, pid, eid, rid string) error {

	// fob it off on the realization policy
	return ReadRealization(caller, pid, eid, rid)
}

func ReadMaterializations(caller *identity.IdentityTraits, pid, eid string) error {

	// fob it off on the realization policy
	return ReadRealizations(caller, pid, eid)
}
