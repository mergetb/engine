package sfe

import (
	"testing"

	xir "gitlab.com/mergetb/xir/v0.3/go"
)

const (
	PHOBOS_BASE              = "facilities/phobos.json"
	PHOBOS_WITH_RESERVATIONS = "facilities/phobos-reservations.json"
	PHOBOS_ROOT              = "ifr"
)

func initFacility(json_model, root_name string) (*xir.Topology, *xir.Device, error) {
	tbx, err := loadFacility(json_model)
	if err != nil {
		return nil, nil, err
	}
	AssignTPAsInfraNet(0x004D000000000000, tbx)
	AssignTPAsXpNet(0x004E000000000000, tbx)

	return tbx, tbx.Device(root_name), nil
}

func test_Experiment(t *testing.T, result ExpectedResult, expected, fac_model, exp_model, root_name string) {
	tbx, root, err := initFacility(fac_model, root_name)
	if err != nil {
		t.Errorf("%+v", err)
		return
	}

	testExperiment(t, tbx, root, result, expected, exp_model)
}

// TestPhobosBase_Hello: establishes the base case for phobos hello_world embedding
// first two nodes (a,b) are mapped to x0, and the third (c) is mapped to x1
func TestPhobosBase_Hello(t *testing.T) {
	expected := `{"Nodes":{"a":"x0 (VirtualMachine)","b":"x0 (VirtualMachine)","c":"x1 (VirtualMachine)"},"Links":{"a.0~b.0~c.0":{"Segments":{"101":{"Endpoints":[{"Host":"a","Ifx":"eth1","Net":"Physical","Ext":"Virtual"},{"Host":"b","Ifx":"eth1","Net":"Physical","Ext":"Virtual"},{"Host":"c","Ifx":"eth1","Net":"Physical","Ext":"Virtual"},{"Host":"x0","Ifx":"ens3","Net":"Vtep","Ext":""},{"Host":"x1","Ifx":"ens3","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"a","Ifx":"eth1","Net":"Tap","Ext":""},{"Host":"b","Ifx":"eth1","Net":"Tap","Ext":""},{"Host":"c","Ifx":"eth1","Net":"Tap","Ext":""},{"Host":"x0","Ifx":"ens3","Net":"Peer","Ext":""},{"Host":"xp","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xp","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"x1","Ifx":"ens3","Net":"Peer","Ext":""}]}}},"infranet":{"Segments":{"100":{"Endpoints":[{"Host":"a","Ifx":"eth0","Net":"Physical","Ext":"Virtual"},{"Host":"b","Ifx":"eth0","Net":"Physical","Ext":"Virtual"},{"Host":"c","Ifx":"eth0","Net":"Physical","Ext":"Virtual"},{"Host":"ifr","Ifx":"ens3","Net":"Vtep","Ext":""},{"Host":"x0","Ifx":"ens2","Net":"Vlan","Ext":""},{"Host":"x1","Ifx":"ens2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"a","Ifx":"eth0","Net":"Tap","Ext":""},{"Host":"b","Ifx":"eth0","Net":"Tap","Ext":""},{"Host":"c","Ifx":"eth0","Net":"Tap","Ext":""},{"Host":"infra","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"infra","Ifx":"","Net":"Vtep","Ext":""},{"Host":"infra","Ifx":"swp1","Net":"Peer","Ext":""},{"Host":"ifr","Ifx":"ens3","Net":"Peer","Ext":""},{"Host":"infra","Ifx":"swp5","Net":"Trunk","Ext":""}]}}}}}`

	test_Experiment(t, Success, expected, PHOBOS_BASE, "experiments/hello_world.json", PHOBOS_ROOT)
}

// TestPhobosWithReservations_Hello: demonstrates how reservations change the base case of hello_world embedding
// each node gets mapped to a separate hypervisor: (a)->x0, (b)->x1, (c)->x2
func TestPhobosWithReservations_Hello(t *testing.T) {
	expected := `{"Nodes":{"a":"x0 (VirtualMachine)","b":"x1 (VirtualMachine)","c":"x2 (VirtualMachine)"},"Links":{"a.0~b.0~c.0":{"Segments":{"101":{"Endpoints":[{"Host":"a","Ifx":"eth1","Net":"Physical","Ext":"Virtual"},{"Host":"b","Ifx":"eth1","Net":"Physical","Ext":"Virtual"},{"Host":"c","Ifx":"eth1","Net":"Physical","Ext":"Virtual"},{"Host":"x0","Ifx":"ens3","Net":"Vtep","Ext":""},{"Host":"x1","Ifx":"ens3","Net":"Vtep","Ext":""},{"Host":"x2","Ifx":"ens3","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"a","Ifx":"eth1","Net":"Tap","Ext":""},{"Host":"b","Ifx":"eth1","Net":"Tap","Ext":""},{"Host":"c","Ifx":"eth1","Net":"Tap","Ext":""},{"Host":"x0","Ifx":"ens3","Net":"Peer","Ext":""},{"Host":"xp","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xp","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"x1","Ifx":"ens3","Net":"Peer","Ext":""},{"Host":"xp","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x2","Ifx":"ens3","Net":"Peer","Ext":""}]}}},"infranet":{"Segments":{"100":{"Endpoints":[{"Host":"a","Ifx":"eth0","Net":"Physical","Ext":"Virtual"},{"Host":"b","Ifx":"eth0","Net":"Physical","Ext":"Virtual"},{"Host":"c","Ifx":"eth0","Net":"Physical","Ext":"Virtual"},{"Host":"ifr","Ifx":"ens3","Net":"Vtep","Ext":""},{"Host":"x0","Ifx":"ens2","Net":"Vlan","Ext":""},{"Host":"x1","Ifx":"ens2","Net":"Vlan","Ext":""},{"Host":"x2","Ifx":"ens2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"a","Ifx":"eth0","Net":"Tap","Ext":""},{"Host":"b","Ifx":"eth0","Net":"Tap","Ext":""},{"Host":"c","Ifx":"eth0","Net":"Tap","Ext":""},{"Host":"infra","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"infra","Ifx":"","Net":"Vtep","Ext":""},{"Host":"infra","Ifx":"swp1","Net":"Peer","Ext":""},{"Host":"ifr","Ifx":"ens3","Net":"Peer","Ext":""},{"Host":"infra","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"infra","Ifx":"swp6","Net":"Trunk","Ext":""}]}}}}}`

	test_Experiment(t, Success, expected, PHOBOS_WITH_RESERVATIONS, "experiments/hello_world.json", PHOBOS_ROOT)
}
