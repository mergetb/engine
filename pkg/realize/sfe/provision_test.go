package sfe

import (
	"testing"

	xir "gitlab.com/mergetb/xir/v0.3/go"
)

// Allocate 1 core on 1 socket against an 8 core dual socket system
func TestResolveProc0(t *testing.T) {

	r := testResource(8, 2)
	s := testSpec(1, 1)
	a := &xir.ProcAllocation{
		Alloc: make(map[uint32]*xir.SocketAllocation),
	}

	a, diags := ResolveProc(r, s, a)

	sa, ok := a.Alloc[0]
	if !ok {
		t.Errorf("expected processor 0 to be allocated")
	}
	if sa.Cores != 1 {
		t.Errorf("expected 1 core allocation on socket 0")
	}

	diags.Dump()

	t.Logf("%s", a)
}

// Allocate 4 cores on 2 sockets against an 8 core dual socket system
func TestResolveProc4x2(t *testing.T) {

	r := testResource(8, 2)
	s := testSpec(4, 2)
	a := &xir.ProcAllocation{
		Alloc: make(map[uint32]*xir.SocketAllocation),
	}

	a, diags := ResolveProc(r, s, a)

	for i := uint32(0); i < 2; i++ {
		sa, ok := a.Alloc[i]
		if !ok {
			t.Errorf("expected processor %d to be allocated", i)
		}
		if sa.Cores != 2 {
			t.Errorf("expected 1 core allocation on socket %d", i)
		}
	}

	diags.Dump()

	t.Logf("%s", a)
}

// Allocate 4 cores on 2 sockets against an 8 core single socket system (failure
// expected)
func TestResolveProc4x2Fail(t *testing.T) {

	r := testResource(8, 1)
	s := testSpec(4, 2)
	a := &xir.ProcAllocation{
		Alloc: make(map[uint32]*xir.SocketAllocation),
	}

	a, diags := ResolveProc(r, s, a)

	if !diags.Error() {
		t.Errorf("expected socket oversubscription")
	}

	t.Logf("%s", a)
}

// helpers --------------------------------------------------------------------

func testResource(cores, sockets uint32) *xir.Resource {

	r := &xir.Resource{
		Id: "test-server",
	}

	for s := uint32(0); s < sockets; s++ {
		r.Procs = append(r.Procs, &xir.Proc{Cores: cores})
	}

	return r

}

func testSpec(cores, sockets uint64) *xir.ProcSpec {

	return &xir.ProcSpec{
		Cores: &xir.Uint32Constraint{
			Op:    xir.Operator_GE,
			Value: uint32(cores),
		},
		Sockets: &xir.Uint32Constraint{
			Op:    xir.Operator_GE,
			Value: uint32(sockets),
		},
	}

}
