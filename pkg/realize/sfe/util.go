package sfe

import (
	"crypto/rand"
	"fmt"
)

func generateUnicastMacAddr() (string, error) {
	buf := make([]byte, 6)
	_, err := rand.Read(buf)
	if err != nil {
		return "", fmt.Errorf("failed to generate random macaddr bytes: %v", err)
	}

	// ensure local+unicast address
	buf[0] = (buf[0] | 2) & 0xfe

	ar := fmt.Sprintf("%02x:%02x:%02x:%02x:%02x:%02x", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5])
	return ar, nil
}
