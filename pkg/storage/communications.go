package storage

import (
	"crypto/sha256"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"sort"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/tech/reconcile"
)

type EMailRecipientType int64

const (
	UNDEFINED EMailRecipientType = iota
	EMAIL_USERS_TYPE
	EMAIL_PROJECT_TYPE
	EMAIL_ORGANIZATION_TYPE
)

func (e EMailRecipientType) String() string {
	switch e {
	case EMAIL_USERS_TYPE:
		return "Users"
	case EMAIL_PROJECT_TYPE:
		return "Project"
	case EMAIL_ORGANIZATION_TYPE:
		return "Organization"
	}
	return "unknown"
}

type EMailRequest struct {
	ToType      EMailRecipientType
	To          []string
	From        string
	Subject     string
	Contents    string
	ContentType string

	ID      string // Unique ID for the email task
	Version int64
}

func NewEMailRequest(
	toType EMailRecipientType, to []string, from, subject, contents, contentType string,
) *EMailRequest {
	er := &EMailRequest{
		ToType:      toType,
		To:          to,
		From:        from,
		Subject:     subject,
		Contents:    contents,
		ContentType: contentType,
	}
	er.Hash()

	return er
}

func NewEMailRequestById(id string) *EMailRequest {
	return &EMailRequest{
		ID: id,
	}
}

func (r *EMailRequest) Bucket() string {
	return "comms/email"
}

func (r *EMailRequest) Id() string {
	return r.ID
}

func (r *EMailRequest) Key() string {
	return "/" + r.Bucket() + "/" + r.Id()
}

func (r *EMailRequest) Value() interface{} {
	return r
}

func (r *EMailRequest) Substrate() Substrate {
	return Etcd
}

func (r *EMailRequest) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func (r *EMailRequest) Unmarshal(b []byte) error {
	return json.Unmarshal(b, r)
}

func (r *EMailRequest) Read() error {
	_, err := etcdTx(readOps(r)...)
	return err
}

func (r *EMailRequest) Exists() bool {
	return r.Version != 0
}

func (r *EMailRequest) GetVersion() int64 {
	return r.Version
}

func (r *EMailRequest) SetVersion(v int64) {
	r.Version = v
}

func (r *EMailRequest) Create() (*Rollback, error) {
	log.Debug("Creating users email storage")

	err := r.Read()
	if err != nil {
		return nil, fmt.Errorf("users email read: %w", err)
	}

	if r.Exists() {
		return nil, fmt.Errorf("email request already exists: cannot create")
	}

	rb, err := etcdTx(writeOps(r)...)
	if err != nil {
		return nil, fmt.Errorf("users email write: %w", err)
	}

	return rb, nil
}

func (r *EMailRequest) Update() (*Rollback, error) {
	return nil, nil
}

func (r *EMailRequest) Delete() (*Rollback, error) {
	err := r.Read()
	if err != nil {
		return nil, fmt.Errorf("users email read: %w", err)
	}

	if !r.Exists() {
		return nil, fmt.Errorf("users email does not exist")
	}

	rb, err := etcdTx(delOps(r)...)
	if err != nil {
		return nil, fmt.Errorf("xdc del: %w", err)
	}

	return rb, nil
}

func (r *EMailRequest) Hash() error {

	toCp := r.To
	sort.Strings(toCp)

	h := sha256.New()
	h.Write([]byte(r.Contents))
	h.Write([]byte(r.Subject))
	h.Write([]byte(strings.Join(toCp, ",")))
	buf := h.Sum(nil)
	r.ID = b64.StdEncoding.EncodeToString(buf)

	return nil
}

func (r *EMailRequest) GetGoal(timeout time.Duration) (*reconcile.TaskForest, error) {

	if r.GetVersion() == 0 {
		err := r.Read()
		if err != nil {
			return nil, err
		}

		if r.GetVersion() == 0 {
			return nil, merror.NotFoundError("email task", r.Id())
		}
	}
	log.Debugf("emailreq: %+v", r)

	tg := reconcile.NewGoal(r.Key(), "EMail: "+r.Id(), "EMail To: "+r.ToType.String()+" "+strings.Join(r.To, ", "))
	trs := FindTaskRecords(r, reconcile.TaskRecord_Exists)
	tg.AddTaskRecord(trs...)

	log.Debugf("tg: %+v", tg)
	log.Debugf("trs: %+v", trs)

	return tg.WatchTaskForest(EtcdClient, timeout)
}
