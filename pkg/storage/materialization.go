package storage

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"time"

	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/portal/services/pkg/merror"

	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/tech/reconcile"
)

const mtzBucketPrefix = "materialize"

type MaterializeRequest struct {
	*portal.MaterializeRequest
}

func NewMaterializeRequest(name, experiment, project string) *MaterializeRequest {

	return &MaterializeRequest{
		MaterializeRequest: &portal.MaterializeRequest{
			Realization: name,
			Project:     project,
			Experiment:  experiment,
		},
	}
}

func (m *MaterializeRequest) Bucket() string {

	return "materializations"

}

func (m *MaterializeRequest) Id() string {

	return fmt.Sprintf("%s/%s/%s", m.Project, m.Experiment, m.Realization)

}

func (m *MaterializeRequest) Key() string {

	return fmt.Sprintf("/%s/%s", m.Bucket(), m.Id())

}

func (m *MaterializeRequest) Value() interface{} {

	return m.MaterializeRequest

}

func (m *MaterializeRequest) Substrate() Substrate {

	return Etcd

}

func (m *MaterializeRequest) Read() error {

	_, err := etcdTx(readOps(m)...)
	return err

}

func (m *MaterializeRequest) Create() (*Rollback, error) {

	if m.Project == "" {
		return nil, fmt.Errorf("project must be specified")
	}
	if m.Experiment == "" {
		return nil, fmt.Errorf("experiment must be specified")
	}
	if m.Realization == "" {
		return nil, fmt.Errorf("realization must have a name")
	}

	r := NewMaterializeRequest(m.Realization, m.Experiment, m.Project)
	err := r.Read()
	if err != nil {
		return nil, fmt.Errorf("read materialization: %v", err)
	}

	rb, err := etcdTx(writeOps(m)...)
	if err != nil {
		return nil, fmt.Errorf("create materialization request write commit: %v", err)
	}

	return rb, nil
}

func (m *MaterializeRequest) Delete() (*Rollback, error) {

	err := m.Read()
	if err != nil {
		return nil, fmt.Errorf("read materialization: %v", err)
	}

	if m.Ver == 0 {
		return nil, fmt.Errorf("materialization does not exist")
	}

	ops, err := m.DeleteLinkedOps()
	if err != nil {
		return nil, fmt.Errorf("mtz del linked ops: %+v", err)
	}

	ops = append(ops, delOps(m)...)

	rb, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("delete mtz request commit: %v", err)
	}

	return rb, nil
}

func (m *MaterializeRequest) GetVersion() int64 {

	return m.Ver
}

func (m *MaterializeRequest) SetVersion(v int64) {

	m.Ver = v
}

func (m *MaterializeRequest) DeleteLinkedOps() ([]StorOp, error) {

	// Currently no linked ops for mtz.
	return []StorOp{}, nil
}

func (m *MaterializeRequest) ToInProgressMaterialization() (*portal.Materialization, error) {
	return &portal.Materialization{
		Rid: m.Realization,
		Eid: m.Experiment,
		Pid: m.Project,
	}, nil
}

func (m *MaterializeRequest) Mzid() string {

	return fmt.Sprintf("%s.%s.%s", m.Realization, m.Experiment, m.Project)

}

func ListMaterializeRequests() ([]*MaterializeRequest, error) {

	result := []*MaterializeRequest{}

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/materializations/", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get materializations: %+v", err)
	}

	for _, kv := range resp.Kvs {

		e := new(portal.MaterializeRequest)
		err = proto.Unmarshal(kv.Value, e)
		if err != nil {
			return nil, fmt.Errorf("malformed materialize request at %s: %+v", string(kv.Key), err)
		}
		result = append(result, &MaterializeRequest{
			MaterializeRequest: e,
		})
	}

	return result, nil
}

func ReadMaterialization(pid, eid, rid string) (*portal.Materialization, error) {

	bucket := bucketName(pid, eid, rid)

	found, err := MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		log.Debugf("bucket exists error: %v", err)
		return nil, status.Error(codes.Internal, err.Error())
	}
	if !found {

		// check to see if the realization request exists, and if so, it's in progress
		rq := NewMaterializeRequest(rid, eid, pid)
		err := rq.Read()
		if err != nil {
			return nil, status.Error(codes.Internal, err.Error())
		}

		// not found at all, so return that
		if rq.GetVersion() == 0 {
			return nil, merror.NotFoundError("Materialization", fmt.Sprintf("%s.%s.%s", rid, eid, pid))
		}

		return nil, merror.MaterializeInProgressError(fmt.Sprintf("%s.%s.%s", rid, eid, pid))
	}

	obj, err := MinIOClient.GetObject(
		context.TODO(),
		bucket,
		"materialization",
		minio.GetObjectOptions{},
	)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	defer obj.Close()

	buf, err := io.ReadAll(obj)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	mz := new(portal.Materialization)
	err = proto.Unmarshal(buf, mz)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return mz, nil

}

func SaveMaterialization(mtz *portal.Materialization) error {

	buf, err := proto.Marshal(mtz)
	if err != nil {
		return fmt.Errorf("marshal materialization: %v", err)
	}

	bucket := bucketName(mtz.Pid, mtz.Eid, mtz.Rid)

	found, err := MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		return fmt.Errorf("minio bucket check: %v", err)
	}
	if !found {
		err := MinIOClient.MakeBucket(
			context.TODO(), bucket, minio.MakeBucketOptions{})
		if err != nil {
			return fmt.Errorf("minio make bucket: %v", err)
		}
	}

	_, err = MinIOClient.PutObject(
		context.TODO(),
		bucket,
		"materialization",
		bytes.NewReader(buf),
		int64(len(buf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("save materialization: %v", err)
	}

	return nil

}

func DeleteMaterialization(pid, eid, rid string) error {

	bucket := bucketName(pid, eid, rid)

	err := MinIOClient.RemoveObject(
		context.TODO(),
		bucket,
		"materialization",
		minio.RemoveObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("delete materialization object: %v", err)
	}

	err = MinIOClient.RemoveBucket(
		context.TODO(),
		bucket,
	)
	if err != nil {
		return fmt.Errorf("delete materialization bucket: %v", err)
	}

	return nil

}

// IsValidMaterializationName - return an error if the given
// component names would create an invalid mtz name.
func IsValidMaterializationName(pid, eid, rid string) error {
	// Bucket name cannot be longer then 63 chars. And we add
	// "materialize-" to the start. We may think about making this
	// just "mz-".
	// There are other char restrictions, but I think the existing Merge API
	// regex is within the set of allowed chars.
	if len(bucketName(pid, eid, rid)) > 63 {
		return fmt.Errorf("Name too long")
	}

	return nil
}

func bucketName(pid, eid, rid string) string {
	return fmt.Sprintf("%s-%s-%s-%s", mtzBucketPrefix, pid, eid, rid)
}

func (m *MaterializeRequest) GetGoal(timeout time.Duration) (*reconcile.TaskForest, error) {

	mzid := fmt.Sprintf("%s.%s.%s", m.Realization, m.Experiment, m.Project)

	// join together the XDCs and the direct MTZ materialization reconcilers
	tg_all := reconcile.NewGoal(m.Key(), "Portal Operations", "All portal operations for "+mzid)

	// get everything directly reconciling on the materialization key
	tg_mtz := reconcile.NewGoal(m.Key(), "Materialization: "+mzid, "Manage materialization: "+mzid)
	tg_mtz.AddTaskRecord(FindTaskRecords(m, reconcile.TaskRecord_Exists)...)

	tf_mtz, err := tg_mtz.WatchTaskForest(EtcdClient, timeout)
	if err != nil {
		return nil, err
	}

	// get XDCs
	/*
		xdcs, err := ListXDCs(m.Project)
		if err != nil {
			return nil, err
		}

			var tfs_xdc []*reconcile.TaskForest

			start := time.Now()
			for _, x := range xdcs {
				if x.Materialization == mzid {
					tf, err := x.GetGoal(timeout - time.Since(start))
					if err != nil {
						return nil, err
					}

					tfs_xdc = append(tfs_xdc, tf)
				}
			}

			// add xdc goal if there's at least 1 of them
			if len(tfs_xdc) > 0 {
				tg_xdcs := reconcile.NewGoal("", "XDCs", "All XDCs attached to "+mzid)
				tf_xdcs := tg_xdcs.JoinTaskForests(tfs_xdc...)

				return tg_all.JoinTaskForests(tf_mtz, tf_xdcs), nil
			}
	*/

	return tg_all.JoinTaskForests(tf_mtz), nil
}

func ToMaterializationSummary(r *portal.Materialization) *portal.MaterializationSummary {
	if r == nil {
		return nil
	}

	return &portal.MaterializationSummary{
		Rid:        r.Rid,
		Pid:        r.Pid,
		Eid:        r.Eid,
		NumMetal:   int32(len(r.Metal)),
		NumVirtual: int32(len(r.Vms)),
		NumLinks:   int32(len(r.Links)),
	}
}
