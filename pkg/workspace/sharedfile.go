package workspace

import (
	"context"
	"fmt"
	"time"

	"github.com/gofrs/flock"
	// log "github.com/sirupsen/logrus"
)

type SharedFile struct {
	Path string
	lock *flock.Flock
}

func SharedOpen(path string) (*SharedFile, error) {

	sf := &SharedFile{
		Path: path,
		lock: flock.New(path + ".lock"),
	}

	locked, err := sf.getLock()
	if err != nil {
		return nil, err
	}
	if !locked {
		return nil, fmt.Errorf("failed to lock file %s: %+v", path, err)
	}

	return sf, nil

}

func (sf *SharedFile) getLock() (bool, error) {

	// try for 30 seconds.
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	locked, err := sf.lock.TryLockContext(ctx, 100*time.Millisecond)

	if err != nil {
		return false, err
	}

	if locked {
		return true, nil
	}

	return false, nil
}

func (sf *SharedFile) Close() {
	sf.lock.Unlock()
}
