package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors"
	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	grpc "google.golang.org/grpc"
)

type grpcStatus string

const (
	StatusFail    grpcStatus = "fail"
	StatusSuccess grpcStatus = "success"
)

type Hook struct {
	URL     string `json:"hook"`
	Message string `json:"message"`
}

// Format for hooks configuraiton is in JSON. It is API centric as top
// level entries are services and methods. After that we have hooks
// for successful method calls and failed method calls. Within the
// hook config itself is the URL for the hook and an optional
// string to add to the hook when called.
//
// Format is => [service][method["success"/"fail"]:hook
// TODO add "after" and "before" hooks. Currently we only invoke a hook
// after the API method is invoked. I could see wanted to call a hook
// before a call.
//
// example:
//
//	{
//	  "portal.v1.Workspace": {
//	  	"GetProject": {
//	  		"success": [{
//	  			"hook": "https://chat.mergetb.net/hooks/ex1qaebfz7gdfezr9z9frmudfa",
//	  			"message": "Hey! Someone wanted to see a project"
//	  		}]
//	  	}
//	  },
//	  "portal.v1.Identity": {
//	  	"Register": {
//	  		"success": [{
//	  		   "hook": "https://chat.mergetb.net/hooks/ex1qaebfz7gdfezr9z9frmudfa",
//	  		   "message": "@here - successful registration. Please inspect and approve if ok."
//	  		}],
//	  		"fail": [{
//	  		   "hook": "https://chat.mergetb.net/hooks/ex1qaebfz7gdfezr9z9frmudfa"
//	  		}]
//	  	},
//	  	"Unregister": {
//	  		"success": [{
//	  		   "hook": "https://chat.mergetb.net/hooks/ex1qaebfz7gdfezr9z9frmudfa",
//	  		   "message": "Say goodbye. 👋"
//	  		}]
//	  	}
//	  }
//	}
type HooksConfig map[string]map[string]map[grpcStatus][]Hook

var (

	// Load hooks confiog from this file.
	hooksConfigPath = "/etc/merge/hooks/hooks.json"

	// The configured hooks.
	hooks *HooksConfig = nil

	// webhook queue
	work *webhookQueue
)

func init() {
	c := getHooks() // will panic on bad config if it exists.
	if c == nil {
		log.Infof("No hooks configured.")
	} else {
		log.Infof("Loaded and validated hooks from %s", hooksConfigPath)
	}

	work = newWebhookQueue(10) // capacity for 10 messages should be enough?

	go work.process()
}

// Load hooks config from file. Validate configuration once loaded. Panic on bad config.
func getHooks() HooksConfig {

	// It's ok for hooks to not be configured.
	if hooks == nil {

		src, err := os.ReadFile(hooksConfigPath)
		if err != nil {
			return nil
		}
		log.Debug("Loaded hooks config from " + hooksConfigPath)

		h := &HooksConfig{}
		err = json.Unmarshal([]byte(src), h)
		if err != nil {
			// May not want to panic here.
			panic(fmt.Sprintf("could not parse hook config file: %v", err))
		}

		err = validateHooksConfig(h)
		if err != nil {
			panic(fmt.Sprintf("invalid hooks config: %v", err))
		}

		hooks = h
	}

	return *hooks
}

// Validate configuration. Only really check that the method statuses are "success" or "fail"
func validateHooksConfig(h *HooksConfig) error {
	for _, service := range *h {
		for _, method := range service {
			for key := range method {
				if key != StatusSuccess && key != StatusFail {
					return fmt.Errorf("status must be one of \"%s\" or \"%s\"", StatusSuccess, StatusFail)
				}
			}
		}
	}
	return nil
}

// This is the function that the GRPC engine calls while processing GRPC messages. This is the function
// that add hooks processing into GRPC middleware. SEarch for this function name in main.go and you can
// see it.
func hookInterceptor(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp any, err error) {

	if hooks == nil {
		return handler(ctx, req)
	}

	// TODO: handle "before" hooks here.

	// Continue the GRPC call and get the outcome.
	m, err := handler(ctx, req)

	// Parse the API path and see if we have a hook configred for this call.
	// Ex format is "/portal.v1.Workspace/GetProject". Split by / into service and api call.
	tokens := strings.Split(info.FullMethod, "/") // use regex?
	if len(tokens) == 3 {
		if method, ok := (*hooks)[tokens[1]]; ok {
			if status, ok := method[tokens[2]]; ok {
				if hooks, ok := status[StatusSuccess]; ok && err == nil {
					for _, hook := range hooks {
						handleAfterSuccessHook(tokens[1], tokens[2], hook, req, m)
					}
				}
				if hooks, ok := status[StatusFail]; ok && err != nil {
					for _, hook := range hooks {
						handleAfterFailHook(tokens[1], tokens[2], hook, req, err)
					}
				}
			}
		}
	}

	return m, err // pass it along
}

// If we have a hook configured for this GRPC call, then return true.
// This is part of the GRPC middle ware. If this funciton "matches" the
// call, then GRPC will invoke the hookInterceptor above.
func hookInterceptorMatch(ctx context.Context, c interceptors.CallMeta) bool {

	if hooks == nil {
		return false
	}

	if svc, ok := (*hooks)[c.Service]; ok {
		if _, ok := svc[c.Method]; ok {
			return true
		}
	}

	return false
}

// Other fields here: https://developers.mattermost.com/integrate/webhooks/incoming/#parameters
type MattermostWebhookData struct {
	Text string `json:"text"`
}

// build the ERROR hook values and queue the data for sending.
func handleAfterFailHook(s, m string, h Hook, request any, e error) {

	d := MattermostWebhookData{
		Text: fmt.Sprintf("Call to `%s/%s` on %s failed at %s: `%s`\n", s, m, api_endpoint, time.Now().Format(time.RFC1123), e.Error()),
	}

	if h.Message != "" {
		d.Text += fmt.Sprintf("Message: %s\n", h.Message)
	}

	d.Text += contentString("Request", cleanRequest(request))

	work.add(webhookData{data: d, hook: h})
}

// build the success hook values and queue the data for sending.
func handleAfterSuccessHook(s, m string, h Hook, request, response any) {

	d := MattermostWebhookData{
		Text: fmt.Sprintf("`%s/%s` called on %s at %s\n", s, m, api_endpoint, time.Now().Format(time.RFC1123)),
	}

	if h.Message != "" {
		d.Text += fmt.Sprintf("Message: %s\n", h.Message)
	}

	d.Text += contentString("Request", cleanRequest(request))
	d.Text += contentString("Response", response)

	work.add(webhookData{data: d, hook: h})
}

// Filter what is shown of the api call arguments.
func cleanRequest(request any) any {

	// Strip password if we're reading a registration.
	if regReq, ok := request.(*portal.RegisterRequest); ok {
		newReq := regReq
		newReq.Password = "****************"
		return newReq
	}

	// Strip password if we're reading a Login.
	if regReq, ok := request.(*portal.LoginRequest); ok {
		newReq := regReq
		newReq.Password = "****************"
		return newReq
	}

	return request
}

func contentString(name string, content any) string {

	var contentBuf bytes.Buffer
	buf, err := json.Marshal(content)
	if err != nil {
		log.Errorf("error unmarshalling content: %s", err)
		return ""
	}

	if len(buf) > 0 {
		err = json.Indent(&contentBuf, buf, "", "\t")
		if err != nil {
			log.Errorf("error indenting json content: %s", err)
			return ""
		}

		s := string(contentBuf.Bytes())

		if s != "{}" {
			return fmt.Sprintf("%s:\n```json\n%s\n```\n", name, string(contentBuf.Bytes()))
		}
	}

	return ""
}

// Actually send the data to the webhook URL
func sendHook(d MattermostWebhookData, h Hook) {

	log.Infof("Sending webhook %v", d)

	jsonData, err := json.Marshal(d)
	if err != nil {
		log.Errorf("unexpected error %v", err)
	}

	client := http.Client{
		Timeout: 10 * time.Second,
	}

	_, err = client.Post(h.URL, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		log.Errorf("unexpected error %v", err)
	}
}

// simple Webhook Queue implementation.
type webhookData struct {
	data MattermostWebhookData
	hook Hook
}

type webhookQueue struct {
	queue chan webhookData
}

func newWebhookQueue(capacity int) *webhookQueue {
	return &webhookQueue{
		queue: make(chan webhookData, capacity),
	}
}

func (wq *webhookQueue) add(data webhookData) {
	select {
	case wq.queue <- data:
		log.Debug("enqueued webhook data")
	default:
		log.Warn("webhook queue is full. dropping outbound webhook call")
	}
}

func (wq *webhookQueue) process() {

	log.Info("Starting webhook processing queue...")

	for data := range wq.queue {
		sendHook(data.data, data.hook)
	}
}
