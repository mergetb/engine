package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_int_auth "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/auth"
	grpc_logging "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/logging"
	grpc_int_selector "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/selector"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	mask "github.com/showa-93/go-mask"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/internal"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"google.golang.org/grpc/reflection"
)

type xps struct{}

func init() {
	internal.InitLogging()

	// Set bootstrap/fallback admin user account.
	value, ok := os.LookupEnv("PORTAL_OPS")
	if ok {
		log.Infof("setting portal OPS to %s", value)
		id.Admin = value
	}

	_, ok = os.LookupEnv("DISABLE_API")
	if ok {
		log.Warnf("Disabling API")
		disabled_api = true
	}

	value, ok = os.LookupEnv("DISABLE_LOGMASKING")
	if ok {
		disabled_masking = true
	}

	value, ok = os.LookupEnv("API_LOGFILE")
	if ok {
		apiLogFile = value
	}

	value, ok = os.LookupEnv("PORTAL_FROM_EMAIL_ADDR")
	if ok {
		portalEMailFromAddr = value
	} else {
		// Too much? We could set a default, but no one would be happy with it.
		log.Infof("PORTAL_FROM_EMAIL_ADDR not set in the environment")
	}

	countries = getCountries()
	usstates = getUSStates()
}

var (
	api_endpoint       string
	grpc_endpoint      string
	sshjump_endpoint   string
	allowed_origins    string
	heartbeat_interval int

	disabled_api     bool
	disabled_masking bool

	apiLogFile string // if set, log GRPC API Calls to the given file, else log to stdout.

	// when the portal sends emails, this is the from address.
	portalEMailFromAddr = "admin@mergetb.net"

	countries []*portal.GetUserConfigurationsResponse_Country
	usstates  []*portal.GetUserConfigurationsResponse_USState
)

var Version = ""

func main() {

	log.Infof("portal version: %s", Version)

	flag.StringVar(&api_endpoint, "api", "", "The REST API endpoint")
	flag.StringVar(&grpc_endpoint, "grpc", "", "The GRPC endpoint")
	flag.StringVar(&sshjump_endpoint, "ssh", "jump.mergetb.net:2022", "The SSH Jump Host endpoint")
	flag.IntVar(&heartbeat_interval, "heartbeat", 60, "Seconds between etcd heartbeats sent")
	flag.StringVar(&allowed_origins, "origins", "https://*.mergetb.net", "Comma-delimited allowed origins for the REST API")

	flag.Parse()

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("storage client init: %v", err)
	}

	err = storage.InitPortalMinIOClient()
	if err != nil {
		log.Fatalf("minio client init: %v", err)
	}

	if api_endpoint == "" {
		log.Fatalf("must specify the api endpoint via the -api argument")
	}

	// https://gitlab.com/mergetb/portal/services/-/issues/291
	_, ssh_port, err := net.SplitHostPort(sshjump_endpoint)
	if ssh_port != "2022" {
		log.Fatalf("at this time, ssh only supports port 2022")
	}

	log.Infof("api endpoint: %s", api_endpoint)
	log.Infof("grpc endpoint: %s", grpc_endpoint)
	log.Infof("SSH jump host endpoint: %s", sshjump_endpoint)
	log.Infof("etcd heartbeat interval: %d seconds", heartbeat_interval)

	l, err := storage.LockPortalConfig()
	if err != nil {
		log.Fatalf("Unable to lock portal config for edit: %s", err)
	}

	pc, err := storage.GetPortalConfig()
	if err != nil {
		log.Fatalf("get portal config: %s", err)
	}

	pc.Config.APIEndpoint = api_endpoint
	pc.Config.GRPCEndpoint = grpc_endpoint
	pc.Config.SSHJumpEndpoint = sshjump_endpoint
	pc.Config.Reconcile.HeartbeatIntervalSec = int64(heartbeat_interval)
	pc.Config.PortalEMailFromAddr = portalEMailFromAddr

	_, err = pc.Update()
	if err != nil {
		log.Fatalf("update portal config: %s", err)
	}

	l.Unlock()

	go runHeartbeater()
	go runGrpc()
	runGrpcGw()
}

func runGrpcGw() {

	log.Infof("Running experimenter gateway on %s", api_endpoint)

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// TODO don't hardcode
	lh := "localhost:6000"

	mux := runtime.NewServeMux()

	opts := []grpc.DialOption{
		grpc.WithInsecure(),
	}

	opts = append(opts, internal.GRPCMaxMessage)
	err := portal.RegisterWorkspaceHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterRealizeHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterMaterializeHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterCommissionHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterCommunicationsHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterAllocHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterIdentityHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterCredHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterXDCHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterWireguardHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterModelHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}

	aos := strings.Split(allowed_origins, ",")
	log.Infof("allowed_origins: %v", aos)

	handler := cors.New(cors.Options{
		AllowedOrigins: aos,
		AllowedMethods: []string{
			http.MethodHead,
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
		},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
	}).Handler(mux)

	handler = checkAuthKratos(handler)

	// This is the port that the container listens on, not the external one.
	log.Fatal(http.ListenAndServe(":8081", handler))

}

func runGrpc() {

	log.Infof("Starting the Merge Portal Experimentation API Server")

	grpc_logging_opts := []grpc_logging.Option{
		grpc_logging.WithLogOnEvents(
			grpc_logging.StartCall,
			grpc_logging.FinishCall,
			grpc_logging.PayloadReceived,
			grpc_logging.PayloadSent,
		),
	}

	// Setup API call logger. Either standard (stdout) or to a separate file. If a separate file, we assume there
	// is a sidecar setup to read the logs.
	// Also we are going to mask certain fields in the logs.  Masking works by adding tags to protobuf
	// messages in api
	mask.RegisterMaskStringFunc(mask.MaskTypeFixed,
		func(arg string, value string) (string, error) {
			return "***", nil
		},
	)
	// masking with some validation: show length of the string
	mask.RegisterMaskStringFunc("len",
		func(arg string, value string) (string, error) {
			return fmt.Sprintf("***(len=%d)***", len(value)), nil
		},
	)
	var apiLogger *log.Logger
	if apiLogFile == "" {
		apiLogger = log.StandardLogger()
	} else {
		apiFile, err := os.OpenFile(apiLogFile, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0600) // RDWR as we want to write without readers
		if err != nil {
			log.Fatalf("opening api log: %+v", err)
		}
		defer apiFile.Close()

		apiLogger = log.New()
		apiLogger.SetOutput(apiFile)
		apiLogger.SetFormatter(&log.JSONFormatter{})
	}

	grpcServer := grpc.NewServer(
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				grpc_logging.StreamServerInterceptor(InterceptorLogger(apiLogger), grpc_logging_opts...),
				grpc_validator.StreamServerInterceptor(),
				// LoginAPI is not a streaming call, no need to make this conditional
				grpc_int_auth.StreamServerInterceptor(disabledApiAuth),
			),
			// NOTE: we do not have hooks middleware for streaming. As far as I know we do not have any
			// streaming API calls. WIll need to revisit when we do.
		),
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				grpc_logging.UnaryServerInterceptor(InterceptorLogger(apiLogger), grpc_logging_opts...),
				grpc_validator.UnaryServerInterceptor(),
				// Apply the disabled API auth interceptor for all API endpoints, but login/logout.
				grpc_int_selector.UnaryServerInterceptor(
					grpc_int_auth.UnaryServerInterceptor(disabledApiAuth),
					grpc_int_selector.MatchFunc(allButLoginAPI),
				),
				grpc_int_selector.UnaryServerInterceptor(
					hookInterceptor,
					grpc_int_selector.MatchFunc(hookInterceptorMatch),
				),
			),
		),
		grpc.MaxRecvMsgSize(internal.GRPCMaxMessageSize),
		grpc.MaxSendMsgSize(internal.GRPCMaxMessageSize),
	)

	portal.RegisterWorkspaceServer(grpcServer, &xps{})
	portal.RegisterRealizeServer(grpcServer, &xps{})
	portal.RegisterMaterializeServer(grpcServer, &xps{})
	portal.RegisterCommissionServer(grpcServer, &xps{})
	portal.RegisterCommunicationsServer(grpcServer, &xps{})
	portal.RegisterAllocServer(grpcServer, &xps{})
	portal.RegisterIdentityServer(grpcServer, &xps{})
	portal.RegisterCredServer(grpcServer, &xps{})
	portal.RegisterXDCServer(grpcServer, &xps{})
	portal.RegisterWireguardServer(grpcServer, &xps{})
	portal.RegisterModelServer(grpcServer, &xps{})

	ep := "0.0.0.0:6000"
	l, err := net.Listen("tcp", ep)
	if err != nil {
		log.Fatalf("failed to listen: %#v", err)
	}

	log.Infof("Listening on tcp://%s", ep)
	reflection.Register(grpcServer)
	grpcServer.Serve(l)
}

func InterceptorLogger(l *log.Logger) grpc_logging.Logger {
	return grpc_logging.LoggerFunc(func(ctx context.Context, lvl grpc_logging.Level, msg string, fields ...any) {
		f := make(map[string]any, len(fields)/2)
		i := grpc_logging.Fields(fields).Iterator()
		for i.Next() {
			k, v := i.At()
			// some fields need to be masked before logging they are tagged "mask"
			if !disabled_masking && (k == "grpc.request.content" || k == "grpc.response.content") {
				if mv, err := mask.Mask(v); err == nil {
					f[k] = mv // masked value
				} else {
					f[k] = "masking failed"
				}
			} else {
				f[k] = v
			}
		}
		l := l.WithFields(f)

		switch lvl {
		case grpc_logging.LevelDebug:
			l.Debug(msg)
		case grpc_logging.LevelInfo:
			l.Info(msg)
		case grpc_logging.LevelWarn:
			l.Warn(msg)
		case grpc_logging.LevelError:
			l.Error(msg)
		default:
			// panic(fmt.Sprintf("unknown level %v", lvl))
		}
	})
}
