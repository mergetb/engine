package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// GetEffectiveGRPCCaller convenience for getEffectiveGRPCCaller
func GetEffectiveGRPCCaller(ctx context.Context) (*identity.IdentityTraits, error) {
	return getEffectiveGRPCCaller(ctx, false)
}

// GetEffectiveGRPCCallerAllowInactive convenience for getEffectiveGRPCCaller
func GetEffectiveGRPCCallerAllowInactive(ctx context.Context) (*identity.IdentityTraits, error) {
	return getEffectiveGRPCCaller(ctx, true)
}

// getEffectiveGRPCCaller is an wrapper around identity.GRPCCaller that allows for sudo
func getEffectiveGRPCCaller(ctx context.Context, allow_inactive bool) (*identity.IdentityTraits, error) {
	caller, err := identity.GRPCCaller(ctx, allow_inactive)
	if err != nil {
		return nil, err
	}
	err = sudoSwitchUser(caller) // changes username when in sudo
	if err != nil {
		return nil, err
	}
	return caller, nil
}

// sudoSwitchUser checks to see if traits.Traits have a record of a sudo request, checks
// if it's allowed and updates the caller's username if it is
func sudoSwitchUser(traits *identity.IdentityTraits) error {
	// see if the user wants to act on behalf of another (sudo); if so, check if allowed
	sudo_username := traits.Traits[portal.GrpcParamSudoUsername]
	if sudo_username != "" && sudo_username != traits.Username {
		log.Debugf("user %s attempted sudo as %s", traits.Username, sudo_username)
		if err := policy.SudoAsUser(traits, sudo_username); err != nil {
			log.Warnf("SudoAsUser policy check failed for user %s, sudo as %s: %s",
				traits.Username, sudo_username, err.Error())
			return fmt.Errorf("Sudo access denied by policy")
		}
		// explicitly check/disallow any sudo to privileged users
		u := storage.NewUser(sudo_username)
		if err := u.Read(); err != nil {
			return fmt.Errorf("Error accessing user %s - %s", sudo_username, err.Error())
		}
		if u.Admin {
			return fmt.Errorf("Sudo access denied - sudo as %s not allowed", sudo_username)
		}

		// just in case save the original user in "sudoer-username" before overwriting
		// xxx not used at the moment, uncomment if needed
		// traits.Traits[portal.GrpcParamSudoerUsername] = traits.Username
		traits.Username = sudo_username
		traits.Email = u.Email
	}
	return nil
}
