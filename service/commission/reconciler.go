package main

import (
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func runReconciler() {

	t := storage.ReconcilerConfigHarbor.ToReconcilerManager(
		&HarborTask{},
	)

	t.Run()
}
