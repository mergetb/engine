package main

import (
	"crypto/tls"
	"fmt"
	"net/mail"
	"sort"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"google.golang.org/grpc"

	gomail "gopkg.in/mail.v2"
)

func sendEmail(r *storage.EMailRequest) error {

	d := gomail.NewDialer(smtpHost, smtpPort, smtpUser, smtpPw)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	addrs, err := getUserEMails(r)
	if err != nil {
		return fmt.Errorf("Unable to read email addresses of the users: %w", err)
	}

	log.Infof("sending email to %s. addrs: %s", r.ToType, strings.Join(addrs, ", "))

	messages := []*gomail.Message{}

	for _, a := range addrs {

		// Sanity check addresses.
		if _, err := mail.ParseAddress(a); err != nil {
			return fmt.Errorf(
				"Invalid address %s found in To list - not sending any email. Error: %+v",
				a, err)
		}

		m := gomail.NewMessage()
		m.SetHeader("From", r.From)
		m.SetHeader("To", a)
		m.SetHeader("Subject", r.Subject)
		m.SetBody(r.ContentType, string(r.Contents))

		messages = append(messages, m)
	}

	// TODO support attachments. There is m.Attach(...)

	return d.DialAndSend(messages...)
}

func getUserEMails(r *storage.EMailRequest) ([]string, error) {

	usernames, err := getUsernames(r)
	if err != nil {
		return nil, fmt.Errorf("Unable to get usernames: %w", err)
	}

	addrs := []string{}

	for _, username := range usernames {

		u := storage.NewUser(username)
		err = u.Read()
		if err != nil {
			return nil, fmt.Errorf("Error reading user data: %w", err)
		}

		if len(u.Email) > 0 {
			log.Debugf("Found email for %s: %s", username, u.Email)
			addrs = append(addrs, u.Email)
		} else {
			log.Warningf("User %s does not have an email address set in the User data", username)
		}
	}

	if len(addrs) == 0 {
		return nil, fmt.Errorf("No emails found for request")
	}

	sort.Strings(addrs)

	return addrs, nil
}

func getUsernames(r *storage.EMailRequest) ([]string, error) {

	if r.ToType == storage.EMAIL_USERS_TYPE {
		return r.To, nil
	}

	var members map[string]*portal.Member

	if r.ToType == storage.EMAIL_PROJECT_TYPE {
		p := storage.NewProject(r.To[0])
		err := p.Read()
		if err != nil {
			return []string{}, fmt.Errorf("error reading project: %s", r.To[0])
		}

		members = p.Members
	} else if r.ToType == storage.EMAIL_ORGANIZATION_TYPE {
		o := storage.NewOrganization(r.To[0])
		err := o.Read()
		if err != nil {
			return []string{}, fmt.Errorf("error reading organization: %s", r.To[0])
		}

		members = o.Members
	}

	if len(members) == 0 {
		return []string{}, fmt.Errorf("No users found")
	}

	u := []string{}
	for m := range members {
		u = append(u, m)
	}

	sort.Strings(u)

	return u, nil
}

func identityClient() (*grpc.ClientConn, portal.IdentityClient, error) {

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", "identity", 6000),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, nil, fmt.Errorf("grpc dial: %v", err)
	}

	return conn, portal.NewIdentityClient(conn), nil

}
