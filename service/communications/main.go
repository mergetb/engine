package main

/*
 *==============================================================================
 * Communications Service
 * ======================
 *
 * The Communications service is a reconciler that supports Communications
 * between portal users.
 *
 *==============================================================================
 */

import (
	"os"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

var (
	smtpHost string
	smtpPort int
	smtpUser string
	smtpPw   string

	smtpEnabled = false
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()

	value, ok := os.LookupEnv("SMTP_ENABLED")
	if ok {
		if value == "true" {
			smtpEnabled = true
		}

		value, ok := os.LookupEnv("SMTP_HOST")
		if ok {
			smtpHost = value
		} else {
			log.Fatalf("SMTP_HOST is not defined in the environment")
		}

		value, ok = os.LookupEnv("SMTP_PORT")
		if ok {
			p, err := strconv.Atoi(value)
			if err != nil {
				log.Fatalf("bad smtp port format at $SMTP_PORT: %s Must be an integer", value)
			}
			smtpPort = p

		} else {
			log.Fatalf("SMTP_PORT is not defined in the environment")
		}

		value, ok = os.LookupEnv("SMTP_PW")
		if ok {
			smtpPw = value
		}

		value, ok = os.LookupEnv("SMTP_USER")
		if ok {
			smtpUser = value
		}
	}
}

var Version = ""

type CommsTask struct {
}

func main() {
	log.Infof("portal version: %s", Version)

	if !smtpEnabled {
		// smtp is the only protocol we support. So just do nothing, I guess.
		// better probably to not install the service at all.
		for {
			log.Warn("Communications service disabled. $SMTP_ENABLED not set to true.")
			time.Sleep(time.Minute * 60)
		}
	} else {

		log.Infof("smtp uri: %s@%s/%s:%d", smtpUser, smtpPw, smtpHost, smtpPort)

		err := storage.InitPortalEtcdClient()
		if err != nil {
			log.Fatalf("etcd client init: %v", err)
		}

		runReconciler()
	}
}
