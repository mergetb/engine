#!/bin/bash

mkdir -p .cert
pushd .cert

cat > ca-csr.json <<EOF
{
  "CN": "mergetb",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "California",
      "O": "mergetb",
      "OU": "CA"
    }
  ]
}
EOF

cat > merge-git-server-csr.json <<EOF
{
  "CN": "mergetb",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "CA",
      "O": "mergetb",
      "OU": "merge-git-server"
    }
  ]
}
EOF

cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "87600h"
    },
    "profiles": {
      "mergetb": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "87600h"
      }
    }
  }
}
EOF

cfssl gencert -initca ca-csr.json | cfssljson -bare ca
cfssl gencert \
    -ca=ca.pem \
    -ca-key=ca-key.pem \
    -config=ca-config.json \
    -hostname=merge-git-server \
    -profile=mergetb \
    merge-git-server-csr.json | cfssljson -bare merge-git-server
