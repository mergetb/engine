package main

import (
	"bytes"
	"fmt"
	"math"
	"strings"
	"text/template"
	"time"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func handleReservationTimeout() {

	log.Infof(
		"Started reservation timeout loop (every %s). Watching for expired realizations.",
		defaultRealizationTimeoutLoopWait,
	)

	for {

		log.Debugf("Starting reservation timeout check.")

		rrs, err := storage.GetAllRealizationRequests()
		if err != nil {
			log.Errorf("Error reading realization requests: %s", err.Error())
		}

		for _, r := range rrs {

			rzid := fmt.Sprintf("%s.%s.%s", r.Realization, r.Experiment, r.Project)

			rlz, err := storage.ReadRealizationResult(r.Project, r.Experiment, r.Realization)
			if err != nil {
				log.Warnf("Unable to read realization in expiration-check loop: %s", rzid)
				continue
			}

			// exp.IsZero() does not work for some reason so check for zeros. nil Expires is for backwards compatibility.
			if rlz.Realization.Expires == nil || (rlz.Realization.Expires.Seconds == 0 && rlz.Realization.Expires.Nanos == 0) {
				log.Debugf("Ignoring realization that does not expire: %s", rzid)
				continue
			}

			now := time.Now()
			exp := rlz.Realization.Expires.AsTime()

			emailExpCheck(now, exp, r.RealizeRequest)

			if exp.Before(now) {
				// It's in the past, kill it.
				log.Infof("Found expired realization: %s. Deleting it.", rzid)

				_, err = r.Delete()
				if err != nil {
					log.Errorf("Error deleting expired realization: %s: %s", rzid, err.Error())
					// Not sure what to do here. We will likely hit this in the next loop as well.
					continue
				}
			}

			// Do we want a between-realizations-expire-check sleep here as well? Will reading all the realization data
			// from MinIO be a tax on the system?
		}

		log.Debugf("Completed reservation timeout check. Sleeping for %s", defaultRealizationTimeoutLoopWait)

		// Wait - then loop again.
		time.Sleep(defaultRealizationTimeoutLoopWait)
	}
}

func emailExpCheck(now, expires time.Time, rq *portal.RealizeRequest) {

	if !rlzExpWarningEnabled {
		return
	}

	rzid := fmt.Sprintf("%s.%s.%s", rq.Realization, rq.Experiment, rq.Project)
	log.Debugf("Checking rlz %s expiration warnings", rzid)

	pc, err := storage.GetPortalConfig()
	if err != nil {
		log.Errorf("Unable to read portal config: %s", err)
		return
	}

	re := storage.NewRealizeExpiration(rq.Realization, rq.Experiment, rq.Project)
	err = re.Read()
	if err != nil {
		log.Warningf("error reading realization expiration data for %s", rzid)
		return
	}
	log.Debugf("read warnings state for %s: %+v", rzid, re.Warnings)

	var expiryDuration time.Duration
	for _, duration := range pc.Config.RlzExpirationWarningDurations {
		if expires.Before(now.Add(duration)) { // if now + duration is past the expires time

			// Have we alerted on this duration already?
			if v, ok := re.Warnings[duration]; ok && v == true {
				log.Debugf("Already alerted on the duration %s for %s, skipping", duration, rzid)
				continue
			}

			// Do we need to lock around this? Yes I think unless we add a distinct update struct like other
			// pkg.storage update functions. TODO
			re.Warnings[duration] = true

			// Check if this warning is sooner - if so use it.
			// We may warn on two or more durations if the user
			// has updated the expiration time to be within two or more
			// durations. This should be rare, but is a corner case.
			// duration in one day and one week. The user changes the expiration
			// to be one hour from now - both durations will be triggered.
			if expiryDuration == 0 {
				expiryDuration = duration
			} else if duration < expiryDuration {
				expiryDuration = duration
			}
		}
	}

	if expiryDuration != 0 {
		log.Infof("Notifying users of realization %s that it is expiring in %s", rzid, expiryDuration)

		err = sendRlzExpirationWarning(rq, expires, expiryDuration, pc.Config.PortalEMailFromAddr)
		if err != nil {
			log.Errorf("Error sending realization warning for %s: %s", rzid, err.Error())
		}

		_, err = re.Update()
		if err != nil {
			log.Errorf("Error updating realization expiration warnings state: %s", err)
		}

		log.Debugf("updated warnings state: %+v", re.Warnings)
	}
}

func sendRlzExpirationWarning(rq *portal.RealizeRequest, expires time.Time, expiryDuration time.Duration, from string) error {

	rzid := fmt.Sprintf("%s.%s.%s", rq.Realization, rq.Experiment, rq.Project)
	expDuration := humanDuration(expiryDuration)
	expTime := expires.Format(time.RFC1123)

	funcMap := template.FuncMap{
		"rlzName":     func() string { return rzid },
		"expDuration": func() string { return expDuration },
		"expTime":     func() string { return expTime },
	}

	tmpl, err := template.New("subject").Funcs(funcMap).Parse(rlzExpEmailTemplate.Subject)
	if err != nil {
		return fmt.Errorf("Error creating subject line template: %w", err)
	}

	var subject bytes.Buffer
	tmpl.Execute(&subject, rq)
	log.Debugf("got subject: %s", subject.String())

	tmpl, err = template.New("body").Funcs(funcMap).Parse(rlzExpEmailTemplate.Body)
	if err != nil {
		return fmt.Errorf("Error creating body template: %w", err)
	}

	var body bytes.Buffer
	tmpl.Execute(&body, rq)
	log.Debugf("got body: %s", body.String())

	log.Infof(
		"sending rlz expiration warning email for %s. expiring in less than %s, at %s",
		rzid, expDuration, expTime,
	)

	// Create a new etcd email entry for emailing the project creators and maintainers.
	// The communications service will pick it up.
	// NOTE: no policy check needed. We are sending as the portal.
	p := storage.NewProject(rq.Project)
	err = p.Read()
	if err != nil {
		return fmt.Errorf("Unable to read proejct %s", rq.Project)
	}

	users := []string{}
	for username, m := range p.Members {
		if m.Role == portal.Member_Creator || m.Role == portal.Member_Maintainer {
			users = append(users, username)
		}
	}

	if len(users) == 0 {
		return fmt.Errorf("Project %s has no creators or maintainers to send mail to", rq.Project)
	}

	e := storage.NewEMailRequest(
		storage.EMAIL_USERS_TYPE,
		users,
		from,
		subject.String(),
		body.String(),
		"text/plain",
	)

	_, err = e.Create()
	if err != nil {
		return fmt.Errorf("Error creating email request: %w", err)
	}

	return nil
}

func humanDuration(duration time.Duration) string {
	days := int64(duration.Hours() / 24)
	hours := int64(math.Mod(duration.Hours(), 24))
	minutes := int64(math.Mod(duration.Minutes(), 60))
	seconds := int64(math.Mod(duration.Seconds(), 60))

	chunks := []struct {
		singularName string
		amount       int64
	}{
		{"day", days},
		{"hour", hours},
		{"minute", minutes},
		{"second", seconds},
	}

	parts := []string{}

	for _, chunk := range chunks {
		switch chunk.amount {
		case 0:
			continue
		case 1:
			parts = append(parts, fmt.Sprintf("%d %s", chunk.amount, chunk.singularName))
		default:
			parts = append(parts, fmt.Sprintf("%d %ss", chunk.amount, chunk.singularName))
		}
	}

	return strings.Join(parts, " ")
}
