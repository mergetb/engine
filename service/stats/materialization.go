package main

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"go.etcd.io/etcd/client/v3"
)

type Mz struct {
	pid string
	eid string
	rid string
}

func parseKey(s string) *Mz {
	// keys are of the form: /materializations/pid/eid/rid

	if !strings.HasPrefix(s, "/materializations/") {
		log.WithFields(log.Fields{"key": s}).Error("can't parse key")
		return nil
	}
	p := strings.Split(string(s)[1:], "/")
	if len(p) != 4 {
		log.WithFields(log.Fields{"key": s}).Error("can't parse key")
		return nil
	}
	return &Mz{pid: p[1], eid: p[2], rid: p[3]}
}

// Read the list of new MZs from etc, and return a map with keys ":pid/:eid/:rid"
func newMzs() map[string]bool {
	resp, err := storage.EtcdClient.KV.Get(context.Background(), "/stats/mz/", clientv3.WithPrefix())
	if err != nil {
		log.WithFields(log.Fields{"err": err, "key": "/stats/mz/"}).Error("etcd get")
		return nil
	}

	out := make(map[string]bool)
	var ops []clientv3.Op
	for _, kv := range resp.Kvs {
		out[string(kv.Key)[10:]] = true // skip "/stats/mz/"
		ops = append(ops, clientv3.OpDelete(string(kv.Key)))
	}

	txn := storage.EtcdClient.Txn(context.Background())
	txnRes, err := txn.If().Then(ops...).Commit()
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("etcd transaction")
		return nil

	}
	if !txnRes.Succeeded {
		log.Error("transaction did not succeed")
		return nil
	}

	return out
}

// Gather stats about materializations
func mzs(projs []string) {

	// fetch the list of new mzs that need to be processed
	mzs := newMzs()

	resp, err := storage.EtcdClient.KV.Get(context.Background(), "/materializations/", clientv3.WithPrefix())
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("etcd get")
		return
	}

	mzcount := make(map[string]int)
	metalcount := make(map[string]int)
	virtcount := make(map[string]int)

	for _, kv := range resp.Kvs {

		k := string(kv.Key)
		s := parseKey(k)
		if s == nil {
			return
		}

		mzcount[s.pid] += 1

		mz, err := storage.ReadMaterialization(s.pid, s.eid, s.rid)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("read materialization")
			continue
		}

		metalcount[s.pid] += len(mz.Metal)
		virtcount[s.pid] += len(mz.Vms)

		if mzs != nil {
			// determine if this mz is new
			if _, ok := mzs[k[len("/materializations/"):]]; ok {
				// new MZ, gather stats about experiment size
				expLinks.With(prometheus.Labels{"project": s.pid}).Observe(float64(len(mz.Links)))
				expMetalNodes.With(prometheus.Labels{"project": s.pid}).Observe(float64(len(mz.Metal)))
				expVirtualNodes.With(prometheus.Labels{"project": s.pid}).Observe(float64(len(mz.Vms)))
			}
		}
	}

	// zero out any projects that were deleted
	for _, p := range projs {
		if _, ok := metalcount[p]; !ok {
			numMzs.With(prometheus.Labels{"project": p}).Set(0.0)
			numMetalNodes.With(prometheus.Labels{"project": p}).Set(0.0)
			numVirtualNodes.With(prometheus.Labels{"project": p}).Set(0.0)
		}
	}

	for p, count := range mzcount {
		numMzs.With(prometheus.Labels{"project": p}).Set(float64(count))
	}

	for p, count := range metalcount {
		numMetalNodes.With(prometheus.Labels{"project": p}).Set(float64(count))
	}

	for p, count := range virtcount {
		numVirtualNodes.With(prometheus.Labels{"project": p}).Set(float64(count))
	}
}

type mztask struct {
	mzid *Mz
}

func (t *mztask) Parse(s string) bool {
	p := parseKey(s)
	if p == nil {
		return false
	}
	t.mzid = p
	return true
}

func (t *mztask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	// store mz creation time

	key := fmt.Sprintf("/stats/start/%s/%s/%s", t.mzid.pid, t.mzid.eid, t.mzid.rid)
	val := fmt.Sprintf("%d", time.Now().Unix())
	_, err := storage.EtcdClient.KV.Put(context.Background(), key, val)
	if err != nil {
		log.WithFields(log.Fields{"err": err, "key": key}).Error("put key")
		return reconcile.TaskMessageError(err)
	}

	totalMzs.With(prometheus.Labels{"project": t.mzid.pid}).Inc()

	/* When the materialization request key is seen, the materialization object
	may not yet be stored in minio, and there is no way to get notified when the
	object is present. Store the mzid in etcd, and have the collect() job fetch
	the information about experiment size for new mzs at a later point. */
	key = fmt.Sprintf("/stats/mz/%s/%s/%s", t.mzid.pid, t.mzid.eid, t.mzid.rid)
	_, err = storage.EtcdClient.KV.Put(context.Background(), key, "")
	if err != nil {
		log.WithFields(log.Fields{"err": err, "key": key}).Error("etcd put")
		return reconcile.TaskMessageError(errors.New("etcd put error"))
	}

	return nil
}

func (t *mztask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	key := fmt.Sprintf("/stats/start/%s/%s/%s", t.mzid.pid, t.mzid.eid, t.mzid.rid)

	resp, err := storage.EtcdClient.KV.Get(context.Background(), key)
	if err != nil {
		log.WithFields(log.Fields{"err": err, "key": key}).Error("get key")
		return reconcile.TaskMessageError(err)
	}

	if len(resp.Kvs) == 0 {
		log.WithFields(log.Fields{"key": key}).Error("no value returned")
		return reconcile.TaskMessageError(errors.New("no creation time found"))
	}

	start, err := strconv.ParseInt(string(resp.Kvs[0].Value), 10, 64)
	if err != nil {
		log.WithFields(log.Fields{"err": err, "val": resp.Kvs[0].Value}).Error("convert to integer")
		return reconcile.TaskMessageError(errors.New("failed integer conversion"))
	}

	// calculate mz run time
	elapsed := time.Now().Unix() - start
	expDuration.With(prometheus.Labels{"project": t.mzid.pid}).Observe(float64(elapsed))

	_, err = storage.EtcdClient.KV.Delete(context.Background(), key)
	if err != nil {
		log.WithFields(log.Fields{"err": err, "key": key}).Error("delete etcd key")
		return reconcile.TaskMessageError(errors.New("delete etcd key"))
	}

	return nil
}

func (t *mztask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return reconcile.TaskMessageUndefined()
}

func (t *mztask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return reconcile.TaskMessageUndefined()
}
