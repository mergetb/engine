package main

import (
	"os"
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()

	n, ok := os.LookupEnv("XDC_NAMESPACE")
	if !ok {
		log.Fatal("XDC_NAMESPACE must be defined in the environment")
	}
	xdcNs = n

	n, ok = os.LookupEnv("WGD_PORT")
	if ok {
		wgdPort = n
	} else {
		log.Info("WGD_PORT not defined, using default 36000")
		wgdPort = "36000"
	}

	n, ok = os.LookupEnv("WG_FORCE_KEY_REDISTRIBUTION")
	if ok && n == "1" {
		forceKeyRedistribution = true
		log.Info("Detected WG_FORCE_KEY_REDISTRIBUTION. All remote peers will be made aware of all keys on all wgif puts and ensures")
	}

}

var (
	Version    = ""
	xdcNs      string
	wgdPort    string
	wgsvcMutex sync.Mutex
)

func main() {
	log.Infof("portal version: %s", Version)

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("etcd client init: %v", err)
	}

	err = storage.InitPortalMinIOClient()
	if err != nil {
		log.Fatalf("minio client init: %v", err)
	}

	runWgifStartup()

	go runPodWatch()
	runReconciler()
}
