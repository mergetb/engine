FROM debian:bullseye-slim

ARG CLI_PROJECT_ID=12314061

RUN apt update && \
        apt-get install -y openssh-server wget supervisor curl jq

USER root

# sshd setup
COPY ./service/xdc/image/common/10-ssh-merge.conf /etc/ssh/sshd_config.d/
RUN chmod 0600 /etc/ssh/sshd_config.d/*
COPY ./service/xdc/image/common/start_sshd.sh /usr/local/sbin/
RUN chmod 0755 /usr/local/sbin/start_sshd.sh
COPY ./service/xdc/image/ssh-jump/sshd_config /etc/ssh/sshd_config
RUN mkdir -p /run/sshd
# this is debian-specific: disable sshd from starting by default
RUN /usr/sbin/update-rc.d ssh disable
RUN touch /etc/ssh/sshd_not_to_be_run

COPY ./build/xdcd build/xdcdcmd /usr/bin/

COPY ./service/xdc/image/ssh-jump/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
# Get latest CLI release version
RUN \
	CLI_VERSION=$(curl -s https://gitlab.com/api/v4/projects/${CLI_PROJECT_ID}/releases/ | jq '.[0].tag_name | .[1:]' -r) && \
	wget https://gitlab.com/mergetb/portal/cli/-/releases/v${CLI_VERSION}/downloads/mrg_${CLI_VERSION}_linux_amd64.deb -O /tmp/mrg.deb && \
	apt install /tmp/mrg.deb && \
	rm /tmp/mrg.deb

# setup sudoers
COPY service/xdc/image/xdc-base/etc--sudoers.d--10-merge /etc/sudoers.d/10-merge
RUN chmod 0600 /etc/sudoers.d/10-merge

RUN date > /created.txt

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
