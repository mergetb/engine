FROM ubuntu:22.04

ARG MARS_VERSION=v1.1.9
ARG CLI_PROJECT_ID=12314061
ARG XIR_PROJECT_ID=8048911

# various packages
RUN apt update && \
    apt-get install --no-install-recommends -y \
    ssh bash-completion openssh-server sudo wget \
    tmux screen curl supervisor vim python3 \
    iputils-ping iproute2 jq dnsmasq

# sshd setup
COPY ./service/xdc/image/common/10-ssh-merge.conf /etc/ssh/sshd_config.d/
RUN chmod 0600 /etc/ssh/sshd_config.d/*
COPY ./service/xdc/image/common/start_sshd.sh /usr/local/sbin/
RUN chmod 0755 /usr/local/sbin/start_sshd.sh
RUN mkdir /run/sshd
#this is ubuntu-specific: disable sshd from starting by default
#RUN update-rc.d ssh disable
RUN touch /etc/ssh/sshd_not_to_be_run

# setup sudoers
COPY service/xdc/image/xdc-base/etc--sudoers.d--10-merge /etc/sudoers.d/10-merge
RUN chmod 0600 /etc/sudoers.d/10-merge

#dnsmasq configuration and scripts
COPY service/xdc/image/xdc-base/etc--dnsmasq.conf /etc/dnsmasq.conf
RUN chown dnsmasq /etc/dnsmasq.conf; chmod 0644 /etc/dnsmasq.conf
COPY service/xdc/image/xdc-base/usr--local--bin--dnsmasq_helper.sh /usr/local/bin/dnsmasq_helper.sh
RUN chmod 0755 /usr/local/bin/dnsmasq_helper.sh

RUN DEBIAN_FRONTEND=noninteractive apt-get update -y
RUN DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y python3-pip git

# jupyterhub
RUN pip3 install jupyterhub
RUN mkdir /etc/jupyter
COPY service/xdc/image/xdc-base/jupyter/jupyterhub_config.py /etc/jupyter/
COPY service/xdc/image/xdc-base/jupyter/merge_jupyterhub/ /tmp/merge_jupyterhub/
RUN pip3 install /tmp/merge_jupyterhub
RUN DEBIAN_FRONTEND=noninteractive apt-get install nodejs npm --fix-missing -y
RUN npm install -g configurable-http-proxy
RUN pip3 install jupyterlab notebook


RUN echo xdc-base $(date) > /created.txt

# Install XIR
WORKDIR /src
RUN git clone https://gitlab.com/mergetb/xir
WORKDIR /src/xir
RUN \
	MX_VERSION=$(curl -s https://gitlab.com/api/v4/projects/${XIR_PROJECT_ID}/releases/ | jq '.[0].tag_name' -r) && \
	git checkout ${MX_VERSION}
WORKDIR /src/xir/v0.3/mx
RUN pip3 install .

# Install merge utilities from CI built locations.
COPY build/xdcd /usr/bin/xdcd
RUN chmod 755 /usr/bin/xdcd
COPY build/xdcdcmd /usr/bin/xdcdcmd
RUN chmod 755 /usr/bin/xdcdcmd
RUN wget https://gitlab.com/mergetb/facility/mars/-/jobs/artifacts/${MARS_VERSION}/raw/build/moacmd?job=make -O /usr/bin/moacmd && chmod 755 /usr/bin/moacmd

# Get latest CLI release version
RUN \
	CLI_VERSION=$(curl -s https://gitlab.com/api/v4/projects/${CLI_PROJECT_ID}/releases/ | jq '.[0].tag_name | .[1:]' -r) && \
	wget https://gitlab.com/mergetb/portal/cli/-/releases/v${CLI_VERSION}/downloads/mrg_${CLI_VERSION}_linux_amd64.deb -O /tmp/mrg.deb && \
	apt install /tmp/mrg.deb && \
	rm /tmp/mrg.deb

COPY service/xdc/image/xdc-base/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

WORKDIR /

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
