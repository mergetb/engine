import os
import os.path
import ory_client
from tornado import web
from jupyterhub.auth import Authenticator
from traitlets import Any

class KratosAuthenticator(Authenticator):

    kratos_api = Any(
        None,
        help="Kratos API endpoint (e.g. http://kratos-public.mergev1). If not set uses environment variable MERGE_KRATOS_API_URL",
    ).tag(config=True)

    def check_allowed(self, username, authentication=None):
        # check if home exists and raise a meaningful exception right away
        if os.path.isdir(f'/home/{username}'):
            return True
        raise web.HTTPError(403, f"local user {username} user doesn't exist")


    async def authenticate(self, handler, data):
        if not self.kratos_api:
            self.kratos_api = os.getenv('MERGE_KRATOS_API_URL')
            if self.kratos_api is None:
                raise web.HTTPError(500, "Kratos api endpoint is not set")

        configuration = ory_client.Configuration(
            host = self.kratos_api,
        )

        with ory_client.ApiClient(configuration) as api_client:
            # Create an instance of the API class
            api_instance = ory_client.FrontendApi(api_client)
            try:
                # Create Login Flow for Native Apps
                api_response = api_instance.create_native_login_flow(refresh=True)
                # Submit a Login Flow
                api_response = api_instance.update_login_flow(
                    api_response.id,
                    ory_client.UpdateLoginFlowBody.from_dict(dict(
                        method='password',
                        identifier=data['username'],
                        password=data['password'],
                    )),
                )
                return api_response.session.identity.traits['username']
            except ory_client.rest.ApiException as ex:
                print(f"Exception: {ex}")
        return None
