package main

import (
	"context"
	"regexp"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/protobuf/proto"
)

var (
	userBucket = storage.PrefixedBucket(&storage.User{})
	userKey    = regexp.MustCompile("^" + userBucket + nameExp + "$")
)

type UserTask struct {
	User string
}

func (ut *UserTask) Parse(key string) bool {

	tnks := userKey.FindAllStringSubmatch(key, -1)
	if len(tnks) > 0 {
		ut.User = tnks[0][1]
		return true
	}

	return false
}

func (ut *UserTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// Only thing we do at the moment is add the user to the ssh-jumps
	u := new(portal.User)
	err := proto.Unmarshal(value, u)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	// Do not add inactive accounts. Return error on non-active accounts.
	if u.State != portal.UserState_Active {
		return reconcile.TaskMessageWarningf("Not reconciling user task for inactive user: %s", u.Username)
	}

	return setupUser(u)
}

func (ut *UserTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// see what changed
	old_u := new(portal.User)
	proto.Unmarshal(prev, old_u) // if this fails, it's OK
	new_u := new(portal.User)
	if err := proto.Unmarshal(value, new_u); err != nil {
		return reconcile.TaskMessageError(err)
	}
	if old_u.State == new_u.State && td.Status.IsReconciled(td.TaskValue) {
		// state hasn't changed, ignore
		log.Info("Ignoring completed user put task: state hasn't changed")
		return nil
	}

	return setupUser(new_u)
}

func (ut *UserTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// Only thing we do at the moment is add the user to the ssh-jumps
	log.Infof("UserTask: ensure not implemented")

	return reconcile.TaskMessageUndefined()
}

func (ut *UserTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	u := new(portal.User)
	err := proto.Unmarshal(value, u)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	js, err := storage.ListSSHJumps()
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	for _, j := range js {

		us := []*portal.User{u}
		err = withXdcdClient(j.Name, func(c xdcd.XdcdClient) error {
			_, err := c.DeleteUsers(
				context.TODO(),
				&xdcd.DeleteUsersRequest{Users: us},
			)
			return err
		})
		if err != nil {
			return reconcile.TaskMessageError(err)
		}
	}

	return nil
}

// setupUser will setup a user at all current jumphosts
// if the user is already set up; it can be used to deactivate
// account if the user is frozen and reactivate it when activated.
func setupUser(u *portal.User) *reconcile.TaskMessage {
	js, err := storage.ListSSHJumps()
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	var ret_err *reconcile.TaskMessage // call to all jumphost, but fail the tast if any one fails
	for _, j := range js {
		err = withXdcdClient(j.Name, func(c xdcd.XdcdClient) error {
			_, err := c.AddUsers(
				context.TODO(),
				&xdcd.AddUsersRequest{
					Users:       []*portal.User{u},
					Sudo:        false,
					Interactive: true, // true as users need a shell to ssh through the jump box
				},
			)
			return err
		})
		if err != nil {
			ret_err = reconcile.TaskMessageError(err)
		}
	}

	return ret_err
}
