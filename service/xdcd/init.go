package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"path"
	"strings"

	log "github.com/sirupsen/logrus"
)

func InitHost() error {

	// now we configure sudo in service/xdc/image/xdc-base.dock

	if err := initMergeGroups(); err != nil {
		return err
	}

	// hopefully the host keys are there.
	if err := restartSSHD(); err != nil {
		return err
	}

	if err := setupDnsmasq(); err != nil {
		return err
	}

	if err := configUserCli("root"); err != nil {
		return err
	}

	if err := configGlobalCli(); err != nil {
		return err
	}

	return nil
}

func restartSSHD() error {

	_, err := exec.LookPath("supervisorctl")
	if err == nil {
		err := exec.Command("supervisorctl", "restart", "sshd").Run()
		if err != nil {
			return handleError(fmt.Errorf("[configSshd] restart supervisord: %v", err))
		}
	} else {
		_, err = exec.LookPath("service")
		if err == nil {
			err := exec.Command("service", "ssh", "restart").Run()
			if err != nil {
				return handleError(fmt.Errorf("[configSshd] service restart sshd: %v", err))
			}
		} else {
			// just kill and restart by hand. (Alpine container)
			_ = exec.Command("pkill", "sshd").Run()
			err = exec.Command("/usr/sbin/sshd", "-e").Run()
			if err != nil {
				return handleError(fmt.Errorf("[configSshd] restart sshd: %v", err))
			}
		}
	}

	return nil
}

func configSudo() error {

	// A better idea is to make a merge group and have all users in that. Alas.

	p := path.Join("/", "etc", "sudoers.d")

	if _, err := os.Stat(p); os.IsNotExist(err) {
		if err = os.Mkdir(p, 0750); err != nil {
			return handleError(fmt.Errorf("[configSudo] can't create %s directory", p))
		}
	}

	conf := "%sudo  ALL=(ALL)       NOPASSWD: ALL"

	p = path.Join(p, "10-merge") // file cannot have . in it or end in ~

	err := os.WriteFile(p, []byte(conf), 0600)
	if err != nil {
		return handleError(fmt.Errorf("[configSudo] write file %s error: %v", p, err))
	}

	return nil
}

func configUserCli(user string) error {

	srv, ok := os.LookupEnv("MERGE_GRPC_SERVER")
	if !ok {
		log.Debugf("MERGE_GRPC_SERVER not set. nothing to do")
		return nil // not really an error
	}

	log.Infof("configuring the merge cli for %s", user)

	mrg, err := exec.LookPath("mrg")
	if err != nil {
		return handleError(fmt.Errorf("[configUserCli] mrg not found in path. unable to configure grcp server: %s", err))
	}

	// check if server is already set. Do not reset or muck with remote mounted home dir by just resetting this
	// all the time.
	o, err := exec.Command("su", user, "-c", "mrg config get server").Output()
	if err != nil {
		return handleError(err)
	}

	existing := strings.Trim(string(o), "\n")

	// Relying on the putput of a mrg command is not great. If not set, the Output
	// is literally ""
	if existing != "\"\"" { // already set. we're done.
		log.Debugf("mrg server already set to %s, returning", existing)
		return nil
	}

	args := []string{
		user, "-c", mrg + " config set server " + srv,
	}

	log.Debugf("running cmd: su %s", strings.Join(args, " "))

	err = exec.Command("su", args...).Run()
	if err != nil {
		return handleError(fmt.Errorf("[configUserCli] Unable to set mrg grpc server: %s", err))
	}

	log.Infof("Set mrg grpc server for %s to %s", user, srv)

	return nil
}

func configGlobalCli() error {

	// This does probably not need to be a fail condition.

	log.Info("configuring the merge cli for all")

	mrg, err := exec.LookPath("mrg")
	if err != nil {
		return handleError(fmt.Errorf("[configGlobalCli] mrg not found in path. unable to configure cli: %s", err))
	}

	cmd := exec.Command(mrg, []string{"completion", "bash"}...)
	if err != nil {
		return handleError(fmt.Errorf("[configGlobalCli] error creating mrg completion command: %s", err))
	}

	var out bytes.Buffer
	cmd.Stdout = &out

	err = cmd.Run()
	if err != nil {
		return handleError(fmt.Errorf("[configGlobalCli] error creating mrg bash completion: %s", err))
	}

	err = os.WriteFile("/etc/bash_completion.d/mrg", out.Bytes(), 0644)
	if err != nil {
		log.Warnf("unable to write mrg bash completion: %s", err)
		// we don't really care if this fails.
	}

	return nil
}
