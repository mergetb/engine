#!/usr/bin/env bash

set -e

export REGISTRY=$(minikube -p $PROFILE ip):5000
export REGISTRY_PATH=mergetb/portal/services/merge
export XDC_PATH=mergetb/portal/services/xdc
export TAG=latest

export DOCKER=podman
export DOCKER_PUSH_ARGS=--tls-verify=false

pushd ..
# ymk build.yml we assume previous stages have built the binaries. 
ymk containers.yml          # tag containers with our $REGISTRY and $REGISTRY_PATH
ymk push-containers.yml     # push containers into the minikube registry
popd

echo Installed containers:
echo minikube IP: $(minikube -p $PROFILE ip)
curl http://$(minikube -p $PROFILE ip):5000/v2/_catalog | jq .

# install the merge portal
export K8S_AUTH_CONTEXT=$PROFILE    # ansible k8s module uses this as the kube context.
ansible-playbook -i inventory -e @extravars.yml merge-portal.yml

